import Vue from 'vue';

const DatabaseIP = 'hindposserver';

export const gqlQuery = async (context, query, local = false) => {
  let uri = `http://${DatabaseIP}:3001/graphql`;
  if (local) {
    uri = 'http://localhost:3001/graphql';
  }
  // eslint-disable-next-line
  // console.log(query);
  const data = await Vue.http
    .post(uri, { query })
    .then((res) => {
      if (res.status !== 200) {
        context.commit('setPopup', {
          title: data.statusText,
          text: `Some unexpected error occured, Error code: ${data.status}.`,
          type: 'error',
        });
      }
      return res.body;
    }, err => err);
  if (data.errors) {
    context.commit('setPopup', {
      title: 'Error',
      text: `An error occured: ${data.errors[0].message}`,
      type: 'error',
    });
  } else if (data.status === 0) {
    context.commit('setPopup', {
      title: 'Server not running',
      text: 'The database server has stopped unexpectedly.',
      type: 'error',
    });
  } else if (data.status === 400) {
    context.commit('setPopup', {
      title: data.statusText,
      text: 'The request was declined (This might be caused due to erroneous data).',
      type: 'error',
    });
  }
  return data;
};

export const printPost = async (dat, pdfOptions, printOptions, local) => {
  let uri = `http://${DatabaseIP}:5000/print`;
  if (local) {
    uri = 'http://localhost:5000/print';
  }
  const result = await Vue.http
    .post(uri, { data: dat, pdfOptions, printOptions })
    .then(res => res.body, err => err);
  return result;
};

export const shutdownGet = async () => {
  const uri = 'http://localhost:5000/shutdown';
  const result = await Vue.http
    .get(uri)
    .then(res => res.body, err => err);
  return result;
};
