import Vue from 'vue';
import VueHighcharts from 'vue-highcharts';
import VueProgress from 'vue-progress';
import shortKey from 'vue-shortkey';
import Notifications from 'vue-notification';
import infiniteScroll from 'vue-infinite-scroll';
// import highchartsMore from 'highcharts/highcharts-more';
// import solidGauge from 'highcharts/modules/solid-gauge';
// import Highcharts from 'highcharts';

import simpleLineIcons from 'simple-line-icons/css/simple-line-icons.css';
import pL from 'professional-look/dist/css/main.css';
import VueResource from 'vue-resource';

import App from './App';

// highchartsMore(Highcharts);
// solidGauge(Highcharts);
Vue.use(VueHighcharts);
Vue.use(VueProgress);
Vue.use(pL);
Vue.use(simpleLineIcons);
Vue.use(VueResource);
Vue.use(shortKey);
Vue.use(Notifications);
Vue.use(infiniteScroll);

Vue.config.productionTip = false;

export { Vue, App };
