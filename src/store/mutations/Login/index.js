const Login = (state) => {
  state.page = 'Dashboard';
  state.loggedIn = true;
  state.sidebarPageActive = 0;
  state.sidebarSubPageActive = 0;
};

const CreateAccount = (state) => {
  state.loginSubPage = 'CreateAccount';
};

const Openlogin = (state) => {
  state.loginSubPage = 'Login';
};
const openCreateAccount = (state) => {
  state.loginSubPage = 'CreateAccount';
};

export default {
  Login,
  CreateAccount,
  Openlogin,
  openCreateAccount,
};
