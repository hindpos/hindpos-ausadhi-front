import _ from 'lodash';

// Create
//  Create

const CreateSubPageFromCreate = (state, products) => {
  const transaction = {};
  transaction.products = _.cloneDeep(products);
  transaction.discount = products
    .map(product => ((product.discount * product.basePrice) * product.quantity) / 10000)
    .map(discount => parseInt(discount, 10))
    .reduce((a, c) => a + c);
  transaction.tax = products
    .map(product => ((product.tax * product.basePrice * (1 - (product.discount / 10000)))
     * product.quantity) / 10000)
    .map(tax => parseInt(tax, 10))
    .reduce((a, c) => a + c);
  transaction.total = products
    .map(product => product.total)
    .reduce((a, c) => a + c);
  transaction.price = transaction.total - transaction.tax;
  transaction.type = 'order';
  transaction.subType = 'create';
  transaction.user = state.user._id;
  transaction.username = state.user.username;
  state.orderCreateTransaction = transaction;
  state.orderCreateSubPage = 'CustomerInfo';
};

//  CustomerInfo
const CreateSubPageFromCustomerInfo = (state) => {
  state.orderCreateSubPage = 'Details';
};

const CreateAddToTransaction = (state, data) => {
  if (data.person) {
    state.orderCreateTransaction.person = _.cloneDeep(data.person);
    if (data.person.address.state !== state.config.address.state) {
      state.orderCreateTransaction.isInterState = true;
    }
  }
  if (data.secondPerson) {
    state.orderCreateTransaction.secondPerson = _.cloneDeep(data.secondPerson);
  }
  if (data.payment) {
    state.orderCreateTransaction.payment = _.cloneDeep(data.payment);
  }
};

const resetTransaction = (state, data) => {
  state.orderCreateTransaction = data;
};

const BackFromCusfo = (state) => {
  state.orderCreateSubPage = 'Create';
};

//  Details
const CreateSubPageFromDetails = (state) => {
  state.orderCreateSubPage = 'Create';
};

// History
//  Details
const HistorySubPageFromDetails = (state) => {
  state.orderHistorySubPage = 'Search';
};
//  Search
const HistorySubPageFromSearch = (state, details) => {
  state.orderHistoryDetails = details;
  state.orderHistorySubPage = 'Details';
};

// Refund
const RefundOrder = (state, order) => {
  state.refundOrder = order;
};

const Active = (state, index) => {
  state.orderRefundIndex = index;
  state.refundOrder = JSON.parse(JSON.stringify(state.refundOrders[index]));
};

const DeleteRow = (state, index) => {
  state.refundOrder.products.splice(index, 1);
};

// Discard
const DiscardProductSearchClick = (state, index) => {
  if (state.orderDiscardProductSearchActive === index) {
    state.orderDiscardProductSearchActive = false;
  } else {
    state.orderDiscardProductSearchActive = index;
  }
};

// Data
// History
const OrderHistory = (state, data) => {
  state.orderHistory = data;
};

export default {
  CreateSubPageFromCreate,
  CreateSubPageFromCustomerInfo,
  BackFromCusfo,
  CreateSubPageFromDetails,
  CreateAddToTransaction,
  resetTransaction,
  HistorySubPageFromDetails,
  HistorySubPageFromSearch,
  RefundOrder,
  Active,
  DeleteRow,
  DiscardProductSearchClick,
  OrderHistory,
};

