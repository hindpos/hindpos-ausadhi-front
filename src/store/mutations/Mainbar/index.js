
const QuickOrder = (state) => {
  state.page = 'QuickOrder';
  state.sidebarPageActive = false;
  state.sidebarSubPageActive = false;
};

const Logout = (state) => {
  state.user = null;
  state.loggedIn = false;
};


const Profile = (state) => {
  state.page = 'User';
  state.sidebarPageActive = false;
  state.sidebarSubPageActive = false;
};

export default {
  QuickOrder,
  Logout,
  Profile,
};
