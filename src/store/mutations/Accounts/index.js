const setDebits = (state, debits) => {
  state.accountsDebits = debits;
};

const setCredits = (state, credits) => {
  state.accountsCredits = credits;
};

const setTransactions = (state, transactions) => {
  state.accountsTransactions = transactions;
};

const setSellStats = (state, sellStats) => {
  state.sellStats = sellStats;
};

const setMeterData = (state, meterData) => {
  state.meterData = meterData;
};

const setChartData = (state, chartData) => {
  state.chartData = chartData;
};

const CreditHistory = (state, data) => {
  state.creditHistory = data;
};

export default {
  setCredits,
  setDebits,
  setTransactions,
  setSellStats,
  setMeterData,
  setChartData,
  CreditHistory,
};
