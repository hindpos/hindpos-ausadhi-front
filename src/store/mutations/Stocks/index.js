import _ from 'lodash';
// Create
//  Create

const SetStockCreateTransaction = (state, transaction) => {
  state.stockCreateTransaction = transaction;
};

const CreateSubPageFromCreate = (state) => {
  state.stockCreateSubPage = 'DistributorInfo';
};

// DisFo
const CreateDisFoTransaction = (state, data) => {
  if (data.person) {
    state.stockCreateTransaction.person = _.cloneDeep(data.person);
    if (data.person.address.state !== state.config.address.state) {
      state.stockCreateTransaction.isInterState = true;
    }
  }
  if (data.payment) {
    state.stockCreateTransaction.payment = _.cloneDeep(data.payment);
  }
};

const CreateTransaction = (state, transaction) => {
  state.stockCreateTransaction = transaction;
};

const CreateSubPagefromDisFo = (state) => {
  state.stockCreateSubPage = 'Details';
};

const BackFromDisfo = (state) => {
  state.stockCreateSubPage = 'Create';
};

//  Details
const CreateSubPageFromDetails = (state) => {
  state.stockCreateSubPage = 'Create';
};

//  Refund
const Active = (state, index) => {
  state.stockRefundIndex = index;
  state.refundStock = JSON.parse(JSON.stringify(state.refundStocks[index]));
};

const DeleteRow = (state, index) => {
  state.refundStock.products.splice(index, 1);
};

//  Search
const HistorySubPageFromSearch = (state, details) => {
  state.stockHistoryDetails = details;
  state.stockHistorySubPage = 'Details';
};

//  Details
const HistorySubPageFromDetails = (state) => {
  state.stockHistorySubPage = 'Search';
};

// Data
// History
const StockHistory = (state, data) => {
  state.stockHistory = data;
};

// Overview
const StockOverview = (state, data) => {
  state.stockOverview = data;
};


export default {
  SetStockCreateTransaction,
  CreateSubPageFromCreate,
  CreateDisFoTransaction,
  CreateSubPageFromDetails,
  CreateSubPagefromDisFo,
  BackFromDisfo,
  CreateTransaction,
  HistorySubPageFromSearch,
  HistorySubPageFromDetails,
  Active,
  DeleteRow,
  StockHistory,
  StockOverview,
};

