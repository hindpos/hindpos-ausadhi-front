// User

const DashboardFromProfile = (state) => {
  state.page = 'Dashboard';
  state.sidebarPageActive = 0;
  state.sidebarSubPageActive = false;
};

export default {
  DashboardFromProfile,
};
