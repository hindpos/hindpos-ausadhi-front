const Click = (state) => {
  state.orderDiscardProductSearchActive = false;
  state.orderCreateProductSearchActive = false;
};

const setUser = (state, user) => {
  state.user = user;
};

const setPopup = (state, popup) => {
  state.popup = popup;
};

export default {
  Click,
  setUser,
  setPopup,
};
