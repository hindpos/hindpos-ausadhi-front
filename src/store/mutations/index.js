import Global from './global';
import Sidebar from './Sidebar';
import Orders from './Orders';
import QuickOrder from './QuickOrder';
import Login from './Login';
import Mainbar from './Mainbar';
import Stocks from './Stocks';
import User from './User';
import Products from './Products';
import Settings from './Settings';
import Notification from './Notification';
import Accounts from './Accounts';

export default {
  // App
  globalClick: Global.Click,
  setPopup: Global.setPopup,

  // User
  setDashboardFromProfile: User.DashboardFromProfile,

  // Accounts
  setAccountsCredits: Accounts.setCredits,
  setAccountsDebits: Accounts.setDebits,
  setAccountsTransactions: Accounts.setTransactions,
  setSellStats: Accounts.setSellStats,
  setMeterData: Accounts.setMeterData,
  setChartData: Accounts.setChartData,

  //  Credit
  setCreditHistory: Accounts.CreditHistory,

  // Sidebar
  sidebarChange: Sidebar.Change,
  sidebarPageChange: Sidebar.PageChange,
  sidebarSubPageChange: Sidebar.SubPageChange,
  setSidebarPages: Sidebar.Page,

  // Order
  //  Create
  // Create
  setOrderCreateSubPageFromCreate: Orders.CreateSubPageFromCreate,
  setOrderCreateSubPageFromCusFo: Orders.CreateSubPageFromCustomerInfo,
  setOrderCreateCusfoTransaction: Orders.CreateAddToTransaction,
  resetOrderCreateTransaction: Orders.resetTransaction,

  // Customerinfo
  setOrderCreateSubPageFromCustomerInfo: Orders.CreateSubPageFromCustomerInfo,
  backFromCusfo: Orders.BackFromCusfo,
  // Details
  setOrderCreateSubPageFromDetails: Orders.CreateSubPageFromDetails,

  //  History
  // Details
  setOrderHistorySubPageFromDetails: Orders.HistorySubPageFromDetails,
  // Search
  setOrderHistorySubPageFromSearch: Orders.HistorySubPageFromSearch,

  setOrderHistory: Orders.OrderHistory,

  //  Refund
  setRefundOrder: Orders.RefundOrder,
  setOrderRefundActive: Orders.Active,
  deleteRowfromOrderRefundProduct: Orders.DeleteRow,

  // Discard
  orderDiscardProductSearchClick: Orders.DiscardProductSearchClick,

  // QuickOrder
  //  Details
  toggleQuickOrderSubPage: QuickOrder.Toggle,
  createQuickOrderTransaction: QuickOrder.CreateTransaction,

  // Products
  setGlobalProducts: Products.GlobalProducts,
  setLocalProducts: Products.LocalProducts,
  setInStockProducts: Products.InStockProducts,
  // Login
  login: Login.Login,
  setUser: Global.setUser,
  // Create Account
  createAccount: Login.CreateAccount,
  openLogInPage: Login.Openlogin,
  // StoreInfo
  openCreateAccount: Login.openCreateAccount,
  // Mainbar
  setPageToQuickOrder: Mainbar.QuickOrder,
  logoutFromMainbar: Mainbar.Logout,
  profileFromMainbar: Mainbar.Profile,

  // Stocks
  //  Create
  //   Create
  setStockCreateTransaction: Stocks.SetStockCreateTransaction,
  setStockCreateSubPageFromCreate: Stocks.CreateSubPageFromCreate,
  setStockCreateDisFoTransaction: Stocks.CreateDisFoTransaction,

  //   Details
  setStockCreateSubPageFromDetails: Stocks.CreateSubPageFromDetails,

  //  Refund
  setStockRefundActive: Stocks.Active,
  deleteRowfromStockRefundProduct: Stocks.DeleteRow,

  //   DistributorInfo
  setStockCreateSubPagefromDisFo: Stocks.CreateSubPagefromDisFo,
  resetStockCreateTransaction: Stocks.CreateTransaction,
  backFromDisfo: Stocks.BackFromDisfo,


  //  History
  //   Search
  setStockHistory: Stocks.StockHistory,
  setStockHistorySubPageFromSearch: Stocks.HistorySubPageFromSearch,

  //   Details
  setStockHistorySubPageFromDetails: Stocks.HistorySubPageFromDetails,

  // Overview
  setStockOverview: Stocks.StockOverview,

  // Notification
  setNotification: Notification.setNotification,
  // Settings
  setConfig: Settings.setConfig,
};
