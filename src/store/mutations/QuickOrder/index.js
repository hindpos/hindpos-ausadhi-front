import _ from 'lodash';

// QuickOrder
// Details
const Toggle = (state, data) => {
  if (state.quickOrderSubPage === 'QuickOrder') {
    state.quickOrderTransaction = _.cloneDeep(data);
    state.quickOrderSubPage = 'Details';
  } else {
    state.quickOrderTransaction = {};
    state.quickOrderSubPage = 'QuickOrder';
  }
};

const CreateTransaction = (state, data) => {
  const transaction = {};
  transaction.products = _.cloneDeep(data.products);
  transaction.discount = data.products
    .map(product => ((product.discount * product.basePrice) * product.quantity) / 10000)
    .map(discount => parseInt(discount, 10))
    .reduce((a, c) => a + c);
  transaction.tax = data.products
    .map(product => ((product.tax * product.basePrice * (1 - (product.discount / 10000)))
     * product.quantity) / 10000)
    .map(tax => parseInt(tax, 10))
    .reduce((a, c) => a + c);
  transaction.total = data.products
    .map(product => product.total)
    .reduce((a, c) => a + c);
  transaction.price = transaction.total - transaction.tax;
  transaction.type = 'order';
  transaction.subType = 'create';
  transaction.user = state.user._id;
  transaction.username = state.user.username;
  state.quickOrderTransaction = transaction;
  transaction.person = { address: { state: data.state } };
  if (state.config.address.state !== data.state) {
    transaction.isInterState = true;
  }
  if (data.customer) {
    transaction.person.name = data.customer;
  }
  if (data.phone) {
    transaction.person.contacts = {};
    transaction.person.contacts.phones = [data.phone];
  }
  if (data.gstin) {
    transaction.person.gstin = data.gstin;
  }
  transaction.payment = {};
  transaction.payment.type = 'cash';
  transaction.payment.closed = true;
  transaction.payment.history = [];
  transaction.payment.history[0] = {
    amount: transaction.total,
    date: (new Date()).toISOString(),
    user: state.user._id,
    username: state.user.username,
  };
  transaction.payment.paid = transaction.total;
  transaction.payment.due = 0;
  if (data.doctor) {
    transaction.secondPerson = {};
    transaction.secondPerson.name = data.doctor;
  }
  state.quickOrderTransaction = _.cloneDeep(transaction);
};

export default {
  Toggle,
  CreateTransaction,
};
