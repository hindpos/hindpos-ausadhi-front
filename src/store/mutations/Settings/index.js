const setConfig = (state, config) => {
  state.config = config;
  state.sidebarType = config.smallSidebar;
};

export default {
  setConfig,
};
