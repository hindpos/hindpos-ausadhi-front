// Create

const GlobalProducts = (state, data) => {
  state.globalProducts = data;
};

const LocalProducts = (state, products) => {
  state.localProducts = products;
};

const InStockProducts = (state, products) => {
  state.inStockProducts = products;
};

export default {
  GlobalProducts,
  LocalProducts,
  InStockProducts,
};
