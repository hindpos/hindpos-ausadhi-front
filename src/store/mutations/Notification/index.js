const setNotification = (state, notification) => {
  state.notification = notification;
};

export default {
  setNotification,
};
