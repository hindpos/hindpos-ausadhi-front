const Change = (state) => {
  state.sidebarType = !state.sidebarType;
};

const PageChange = (state, index) => {
  if (state.sidebarPages[index].name) {
    state.page = state.sidebarPages[index].name;
    if (state.sidebarPages[index].subPages) {
      const subPageActive = state.sidebarPages[index].subPageActive;
      if (state.sidebarPages[index].subPages[subPageActive].name) {
        state.subPage = state.sidebarPages[index].subPages[subPageActive].name;
        state.sidebarSubPageActive = subPageActive;
      }
    }
  }
  state.sidebarPageActive = index;
};

const SubPageChange = (state, index) => {
  state.sidebarSubPageActive = index;
  if (state.sidebarPages[state.sidebarPageActive].subPages) {
    if (state.sidebarPages[state.sidebarPageActive].subPages[index].name) {
      state.subPage = state.sidebarPages[state.sidebarPageActive].subPages[index].name;
      state.sidebarPages[state.sidebarPageActive].subPageActive = index;
    }
  }
};

const Page = (state, user) => {
  if (!user.admin) {
    state.sidebarPages = [
      {
        admin: false,
        name: 'Dashboard',
        icon: 'dashboard',
        index: 0,
      },
      {
        admin: false,
        name: 'Orders',
        icon: 'order',
        index: 1,
        subPages: [
          {
            name: 'Create',
            icon: 'create',
            index: 0,
          },
          {
            name: 'Refund',
            icon: 'refund',
            index: 1,
          },
          {
            name: 'History',
            icon: 'history',
            index: 2,
          },
        ],
        subPageActive: 0,
      },
      {
        admin: false,
        name: 'Products',
        icon: 'product',
        index: 2,
        subPages: [
          {
            name: 'Overview',
            icon: 'overview',
            index: 0,
          },
        ],
        subPageActive: 0,
      },
      {
        admin: false,
        name: 'Stocks',
        icon: 'stock',
        index: 3,
        subPages: [
          {
            name: 'Overview',
            icon: 'overview',
            index: 0,
          },
          {
            name: 'History',
            icon: 'history',
            index: 1,
          },
        ],
        subPageActive: 0,
      },
    ];
  } else {
    state.sidebarPages = [
      {
        admin: false,
        name: 'Dashboard',
        icon: 'dashboard',
        index: 0,
      },
      {
        admin: false,
        name: 'Orders',
        icon: 'order',
        index: 1,
        subPages: [
          {
            name: 'Create',
            icon: 'create',
            index: 0,
          },
          {
            name: 'Refund',
            icon: 'refund',
            index: 1,
          },
          {
            name: 'Discard',
            icon: 'discard',
            admin: true,
            index: 2,
          },
          {
            name: 'History',
            icon: 'history',
            index: 3,
          },
        ],
        subPageActive: 0,
      },
      {
        admin: false,
        name: 'Products',
        icon: 'product',
        index: 2,
        subPages: [
          {
            name: 'Overview',
            icon: 'overview',
            index: 0,
          },
          {
            name: 'Create',
            icon: 'create',
            admin: false,
            index: 1,
          },
          {
            name: 'Edit',
            icon: 'edit',
            admin: false,
            index: 2,
          },
        ],
        subPageActive: 0,
      },
      {
        admin: false,
        name: 'Stocks',
        icon: 'stock',
        index: 3,
        subPages: [
          {
            name: 'Overview',
            icon: 'overview',
            index: 0,
          },
          {
            name: 'Create',
            icon: 'create',
            admin: true,
            index: 1,
          },
          // {
          //   name: 'Refund',
          //   icon: 'refund',
          // },
          {
            name: 'History',
            icon: 'history',
            index: 2,
          },
        ],
        subPageActive: 0,
      },
      // {
      //   name: 'Analytics',
      //   icon: 'analytics',
      // },
      {
        admin: false,
        name: 'Notification',
        icon: 'notification',
        index: 4,
      },
      {
        admin: true,
        index: 5,
        name: 'Reports',
        icon: 'report',
      },
      // {
      //   name: 'Administrator',
      //   icon: 'admin',
      // },
      {
        name: 'Accounts',
        icon: 'accounts',
        admin: true,
        index: 6,
        subPages: [
          {
            name: 'Overview',
            icon: 'overview',
            index: 0,
          },
          {
            name: 'Timeline',
            icon: 'history',
            index: 1,
          },
          {
            name: 'Credits',
            icon: 'create',
            index: 2,
          },
          {
            name: 'Debits',
            icon: 'refund',
            index: 3,
          },
        ],
        subPageActive: 0,
      },
      {
        admin: true,
        name: 'Settings',
        icon: 'settings',
        index: 7,
      },
    ];
  }
};

export default {
  Change,
  PageChange,
  SubPageChange,
  Page,
};
