import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    // App State
    page: 'Dashboard',
    subPage: '',
    loggedIn: false,

    // Login
    loginSubPage: 'Login',

    // Sidebar State
    sidebarType: false,
    sidebarPages: null,
    sidebarPageActive: 0,
    sidebarSubPageActive: 0,

    // QuickOrder
    quickOrderSubPage: 'QuickOrder',
    quickOrderTransaction: null,

    // Order
    //  Create
    orderCreateSubPage: 'Create',
    localProducts: [],
    orderCreateTransaction: null,

    //  Refund
    orderRefundIndex: 0,
    refundOrder: null,
    //  Discard
    orderDiscardProductSearchActive: false,

    //  History
    orderHistory: null,
    orderHistoryDetails: null,
    orderHistorySubPage: 'Search',

    // Products
    globalProducts: null,
    inStockProducts: null,

    // Stock
    //  Create
    stockCreateSubPage: 'Create',
    stockCreateTransaction: null,
    //  History
    stockHistory: null,
    stockOverview: null,
    stockHistoryDetails: null,
    stockHistorySubPage: 'Search',

    // Settings
    config: {
      autoBill: false,
      smallSidebar: false,
      expiryWarn: 7,
      numCopies: 1,
      printType: 'Thermal',
      target: {
        profit: 1000,
        revenue: 10000,
        order: 100,
      },
      store: {
        gstin: null,
        id: null,
        license: null,
        name: null,
        owner: null,
      },
      address: {
        pincode: null,
        city: null,
        state: '19 - West Bengal',
        country: 'India',
        landmark: null,
        lines: [],
      },
    },

    // Accounts
    accountsDebits: [],
    accountsCredits: [],
    accountsTransactions: [],

    // Data
    orderHistoryDetailsLimit: 50,
    orderHistoryDetailsDisplay: 50,
    user: null,
    sellStats: null,
    meterData: null,
    chartData: null,
    notification: [],
    popup: null,
    gstPercent: [0, 5, 12, 18, 28],
    indianStates: [
      '19 - West Bengal',
      '35 - Andaman and Nicobar Islands',
      '37 - Andhra Pradesh',
      '12 - Arunachal Pradesh',
      '18 - Assam',
      '10 - Bihar',
      '04 - Chandigarh',
      '22 - Chattisgarh',
      '26 - Dadra and Nagar Haveli',
      '25 - Daman and Diu',
      '07 - Delhi',
      '30 - Goa',
      '24 - Gujarat',
      '06 - Haryana',
      '02 - Himachal Pradesh',
      '01 - Jammu and Kashmir',
      '20 - Jharkhand',
      '29 - Karnataka',
      '32 - Kerala',
      '31 - Lakshadweep Islands',
      '23 - Madhya Pradesh',
      '27 - Maharashtra',
      '14 - Manipur',
      '17 - Meghalaya',
      '15 - Mizoram',
      '13 - Nagaland',
      '21 - Odisha',
      '34 - Pondicherry',
      '03 - Punjab',
      '08 - Rajasthan',
      '11 - Sikkim',
      '33 - Tamil Nadu',
      '36 - Telangana',
      '16 - Tripura',
      '09 - Uttar Pradesh',
      '05 - Uttarakhand',
    ],
  },
  getters,
  mutations,
  actions,
});

export default store;
