const type = state => state.sidebarType;

const Pages = state => state.sidebarPages;

const PageActive = state => state.sidebarPageActive;

const SubPageActive = state => state.sidebarSubPageActive;

export default {
  type,
  Pages,
  PageActive,
  SubPageActive,
};
