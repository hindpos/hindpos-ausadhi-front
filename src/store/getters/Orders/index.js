// Create
const createSubPage = state => state.orderCreateSubPage;

const createTransaction = state => state.orderCreateTransaction;

// Discard
const discardProductSearchActive = state =>
  state.orderDiscardProductSearchActive;

// Refund
const refundOrders = state =>
  state.refundOrders;

const refundOrder = state =>
  state.refundOrder;

const refundIndex = state =>
  state.orderRefundIndex;

// History
const orderHistory = state =>
  state.orderHistory;

const historyDetails = state =>
  state.orderHistoryDetails;

const historySubPage = state =>
  state.orderHistorySubPage;

export default {
  createTransaction,
  createSubPage,
  discardProductSearchActive,
  refundOrders,
  refundOrder,
  refundIndex,
  orderHistory,
  historyDetails,
  historySubPage,
};
