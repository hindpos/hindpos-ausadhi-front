const getGlobalProducts = state => state.globalProducts;

const getLocalProducts = state => state.localProducts;

const getInStockProducts = state => state.inStockProducts;

export default {
  getGlobalProducts,
  getLocalProducts,
  getInStockProducts,
};
