const getConfig = state => state.config;

export default {
  getConfig,
};
