const SubPage = state => state.quickOrderSubPage;

const Transaction = state => state.quickOrderTransaction;

export default {
  SubPage,
  Transaction,
};
