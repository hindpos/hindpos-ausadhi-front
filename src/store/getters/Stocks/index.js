// Stocks Getters
//  Create

const CreateSubPage = state => state.stockCreateSubPage;
const CreateTransaction = state => state.stockCreateTransaction;
//  History
const HistorySubPage = state => state.stockHistorySubPage;

// History
const stockHistory = state =>
  state.stockHistory;

const historyDetails = state =>
  state.stockHistoryDetails;

// Overview
const stockOverview = state =>
  state.stockOverview;

export default {
  CreateSubPage,
  HistorySubPage,
  CreateTransaction,
  stockHistory,
  stockOverview,
  historyDetails,
};
