import Global from './global';
import Orders from './Orders';
import Sidebar from './Sidebar';
import Mainbar from './Mainbar';
import QuickOrder from './QuickOrder';
import Stocks from './Stocks';
import Login from './Login';
import Products from './Products';
import Settings from './Settings';
import Notification from './Notification';
import Accounts from './Accounts';

export default {
  // Global
  getPage: Global.getPage,
  getSubPage: Global.getSubPage,
  isLoggedIn: Global.isLoggedIn,
  getUser: Global.getUser,
  getPopup: Global.getPopup,
  getGstPercent: Global.getGstPercent,
  getIndianStates: Global.getIndianStates,

  // LogIn
  getLoginSubPage: Login.subPage,

  // Accounts
  getAccountsCredits: Accounts.getCredits,
  getAccountsDebits: Accounts.getDebits,
  getAccountsTransactions: Accounts.getTransactions,
  getSellStats: Accounts.getSellStats,
  getMeterData: Accounts.getMeterData,
  getChartData: Accounts.getChartData,

  // Order
  //  Create
  getOrderCreateTransaction: Orders.createTransaction,
  getOrderCreateSubPage: Orders.createSubPage,

  //  Discard
  getOrderDiscardProductSearchActive: Orders.discardProductSearchActive,

  //  Refund
  getRefundOrders: Orders.refundOrders,
  getRefundOrder: Orders.refundOrder,
  getOrderRefundIndex: Orders.refundIndex,

  //  History
  getOrderHistory: Orders.orderHistory,
  getOrderHistoryDetails: Orders.historyDetails,
  getOrderHistorySubPage: Orders.historySubPage,

  // Sidebar
  getSidebarType: Sidebar.type,
  getSidebarPages: Sidebar.Pages,
  getSidebarPageActive: Sidebar.PageActive,
  getSidebarSubPageActive: Sidebar.SubPageActive,

  // Mainbar
  getProfileFromMainbar: Mainbar.ProfileFromMainbar,

  // QuickOrder
  getQuickOrderSubPage: QuickOrder.SubPage,
  getQuickOrderTransaction: QuickOrder.Transaction,

  // Product
  getGlobalProducts: Products.getGlobalProducts,
  getLocalProducts: Products.getLocalProducts,
  getInStockProducts: Products.getInStockProducts,

  // Stock
  //  Create
  getStockCreateSubPage: Stocks.CreateSubPage,
  getStockCreateTransaction: Stocks.CreateTransaction,

  //  History
  getStockHistory: Stocks.stockHistory,
  getStockHistoryDetails: Stocks.historyDetails,
  getStockHistorySubPage: Stocks.HistorySubPage,

  // Overview
  getStockOverview: Stocks.stockOverview,

  // Notification
  getNotification: Notification.getNotification,
  // Settings
  getConfig: Settings.getConfig,
};
