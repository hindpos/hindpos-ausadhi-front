
const getPage = state => state.page;
const getSubPage = state => state.subPage;
const isLoggedIn = state => state.loggedIn;
const getUser = state => state.user;
const getPopup = state => state.popup;
const getGstPercent = state => state.gstPercent;
const getIndianStates = state => state.indianStates;

export default {
  getPage,
  getSubPage,
  isLoggedIn,
  getUser,
  getPopup,
  getGstPercent,
  getIndianStates,
};
