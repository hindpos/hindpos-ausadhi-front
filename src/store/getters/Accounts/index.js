const getDebits = state => state.accountsDebits;

const getCredits = state => state.accountsCredits;

const getTransactions = state => state.accountsTransactions;

const getSellStats = state => state.sellStats;

const getMeterData = state => state.meterData;

const getChartData = state => state.chartData;

export default {
  getCredits,
  getDebits,
  getTransactions,
  getSellStats,
  getMeterData,
  getChartData,
};
