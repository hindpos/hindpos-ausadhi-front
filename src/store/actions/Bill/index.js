import _ from 'lodash';
import { printPost } from '@/services/Api';

const pdfPrintOrder = async (order, config) => {
  const store = _.cloneDeep(config.store);
  store.address = config.address;
  const copyOrder = _.pick(order, ['num', 'price', 'tax', 'total', 'person', 'secondPerson', 'discount']);
  copyOrder.payment = {};
  copyOrder.payment.paid = (order.payment.paid / 100).toFixed(2);
  copyOrder.payment.due = (order.payment.due / 100).toFixed(2);
  copyOrder.price = (copyOrder.price / 100).toFixed(2);
  copyOrder.tax = (copyOrder.tax / 100).toFixed(2);
  copyOrder.total = (copyOrder.total / 100).toFixed(2);
  copyOrder.updatedAt = (new Date()).toLocaleDateString();
  copyOrder.products = order.products.map((prod) => {
    const copyProd = _.pick(prod, ['name', 'quantity', 'hsn', 'discount']);
    const rate = (prod.sellPrice / (1 + (prod.tax / 10000)));
    copyProd.rate = (rate / 100).toFixed(2);
    copyProd.discount = (copyProd.discount / 100).toFixed(2);
    copyProd.subTotal = ((rate * prod.quantity) / 100).toFixed(2);
    copyProd.total = (prod.total / 100).toFixed(2);
    copyProd.taxes = {};
    copyProd.taxes.cgst = {
      percent: (prod.tax / 200).toFixed(2),
      amount: ((prod.total / 200) - (prod.quantity * (rate / 200))).toFixed(2),
    };
    copyProd.taxes.sgst = {
      percent: (prod.tax / 200).toFixed(2),
      amount: ((prod.total / 200) - (prod.quantity * (rate / 200))).toFixed(2),
    };
    return copyProd;
  });
  await printPost({
    order: copyOrder,
    store,
  }, {
    pageSize: config.printType,
  },
  {
    media: config.printType,
    // 'fit-to-page': true,
  },
  true);
};

const txtPrintRefund = async (context, order, config) => {
  const products = order.products
    .filter(prod => prod.refund)
    .map(each => _.pick(each, ['name', 'netPrice', 'refund']));
  const nameArr = [];
  const quantityArr = [];
  const totalArr = [];
  const nameS = 15;
  const quantityS = 5;
  const totalS = 10;
  products.forEach((each) => {
    let { name } = each;
    const { refund } = each;
    name = name.toString();
    let quantity = refund.refund.toString();
    let total = `${(refund.amount / 100).toFixed(2).toString()}`;
    while ((name + quantity + total).length > 0) {
      nameArr.push(name.slice(0, nameS));
      quantityArr.push(quantity.slice(0, quantityS));
      totalArr.push(total.slice(0, totalS));
      name = name.slice(nameS);
      quantity = quantity.slice(quantityS);
      total = total.slice(totalS);
    }
    nameArr.push('');
    quantityArr.push('');
    totalArr.push('');
  });
  let string = '';
  if (order) {
    if (order.num) {
      string = `${string}${(`REFUND #${order.num}`).padEnd(nameS + 1)}        ${(new Date(order.createdAt)).toLocaleDateString().padEnd(12)}\n\n\n`;
    }
  }
  if ('total' in order) {
    const total = `₹ ${(order.total / 100).toFixed(2).toString()}`;
    string = `${string}${'TOTAL'.padEnd(nameS + quantityS)}  ${total.padEnd(totalS)}\n\n\n`;
  }
  if (config.store) {
    if (config.store.gstin) {
      string = `${string}${(`GSTIN ${config.store.gstin}`).padEnd(nameS + quantityS + totalS)}\n`;
    }
    if (config.store.name) {
      string = `${string}${config.store.name.padEnd(nameS + quantityS + totalS)}\n`;
    }
  }
  if (config.address) {
    if (Array.isArray(config.address.lines)) {
      config.address.lines.forEach((line) => {
        string = `${string}${line.padEnd(nameS + quantityS + totalS)}\n`;
      });
    }
    if (config.address.city || config.address.pincode) {
      string = `${string}${(`${config.address.city} ${config.address.pincode}`).padEnd(nameS + quantityS + totalS)}\n\n\n`;
    }
  }

  string = `${string}${'NAME'.padEnd(nameS)}  ${'QTY'.padEnd(quantityS)}${'TAX'.padEnd(quantityS)}  ${'TOTAL'.padEnd(totalS)}\n\n`;
  const tot = nameArr.length;
  for (let i = 0; i < tot; i += 1) {
    string = `${string}${nameArr.shift().padEnd(nameS)}  ${quantityArr.shift().padEnd(quantityS)} ${totalArr.shift().padEnd(totalS)}\n`;
  }
  const totalRef = products.map(p => p.refund.amount).reduce((a, c) => a + c);
  const total = `₹ ${(totalRef / 100).toFixed(2).toString()}`;
  string = `${string}${'TOTAL'.padEnd(nameS + quantityS)}  ${total.padEnd(totalS)}\n`;

  string = `${string}\n\nInvoice by Hindpos.com\n\n`;
  const numCopies = config.numCopies || 1;
  const forAwait = async (func, inputs, n) => {
    if (n > 0) {
      await func(...inputs);
      forAwait(func, inputs, (n - 1));
    }
  };
  forAwait(printPost, [
    string,
    {
      pageSize: config.printType,
    },
    {},
    true,
  ], numCopies);
  return 0;
};

const txtPrintOrder = async (context, order, config) => {
  const products = order.products
    .map(each => _.pick(each, ['name', 'quantity', 'basePrice', 'discount', 'tax', 'total']));
  const nameArr = [];
  const quantityArr = [];
  const taxArr = [];
  const totalArr = [];
  const nameS = 12;
  const quantityS = 5;
  const taxS = 3;
  const totalS = 10;
  products.forEach((each) => {
    let { name, quantity, tax, total } = each;
    tax = `${(tax / 100).toString()}`;
    name = name.toString();
    quantity = quantity.toString();
    total = `${(total / 100).toFixed(2).toString()}`;
    while ((name + quantity + total).length > 0) {
      nameArr.push(name.slice(0, nameS));
      quantityArr.push(quantity.slice(0, quantityS));
      taxArr.push(tax.slice(0, taxS));
      totalArr.push(total.slice(0, totalS));
      name = name.slice(nameS);
      quantity = quantity.slice(quantityS);
      tax = tax.slice(taxS);
      total = total.slice(totalS);
    }
    nameArr.push('');
    quantityArr.push('');
    taxArr.push('');
    totalArr.push('');
  });
  let string = '';
  if (order) {
    if (order.num) {
      string = `${string}${(`ORDER #${order.num}`).padEnd(nameS + 1)}        ${(new Date()).toLocaleDateString().padEnd(12)}\n\n\n`;
    }
  }
  if ('total' in order) {
    const total = `₹ ${(order.total / 100).toFixed(2).toString()}`;
    string = `${string}${'TOTAL'.padEnd(nameS + quantityS + taxS)}  ${total.padEnd(totalS)}\n\n\n`;
  }
  if (config.store) {
    if (config.store.gstin) {
      string = `${string}${(`GSTIN ${config.store.gstin}`).padEnd(nameS + quantityS + taxS + totalS)}\n`;
    }
    if (config.store.name) {
      string = `${string}${config.store.name.padEnd(nameS + quantityS + taxS + totalS)}\n`;
    }
  }
  if (config.address) {
    if (Array.isArray(config.address.lines)) {
      config.address.lines.forEach((line) => {
        string = `${string}${line.padEnd(nameS + quantityS + taxS + totalS)}\n`;
      });
    }
    if (config.address.city || config.address.pincode) {
      string = `${string}${(`${config.address.city} ${config.address.pincode}`).padEnd(nameS + quantityS + taxS + totalS)}\n\n\n`;
    }
  }

  string = `${string}${'NAME'.padEnd(nameS)}  ${'QTY'.padEnd(quantityS)}${'TAX'.padEnd(taxS)}  ${'TOTAL'.padEnd(totalS)}\n\n`;
  const tot = nameArr.length;
  for (let i = 0; i < tot; i += 1) {
    string = `${string}${nameArr.shift().padEnd(nameS)}  ${quantityArr.shift().padEnd(quantityS)}${taxArr.shift().padEnd(taxS)}  ${totalArr.shift().padEnd(totalS)}\n`;
  }
  if ('discount' in order) {
    const discount = `₹ ${(order.discount / 100).toFixed(2).toString()}`;
    string = `${string}${'DISCOUNT'.padEnd(nameS + quantityS + taxS)}  ${discount.padEnd(totalS)}\n`;
  }
  if ('tax' in order) {
    const cgst = `₹ ${(order.tax / 200).toFixed(2).toString()}`;
    const tax = `₹ ${(order.tax / 100).toFixed(2).toString()}`;
    string = `${string}${'CGST'.padEnd(nameS + quantityS + taxS)}  ${cgst.padEnd(totalS)}\n`;
    string = `${string}${'SGST'.padEnd(nameS + quantityS + taxS)}  ${cgst.padEnd(totalS)}\n`;
    string = `${string}${'GST'.padEnd(nameS + quantityS + taxS)}  ${tax.padEnd(totalS)}\n`;
  }
  if ('total' in order) {
    const total = `₹ ${(order.total / 100).toFixed(2).toString()}`;
    string = `${string}${'TOTAL'.padEnd(nameS + quantityS + taxS)}  ${total.padEnd(totalS)}\n`;
  }
  if ('payment' in order) {
    if ('paid' in order.payment) {
      const paid = `₹ ${(order.payment.paid / 100).toFixed(2).toString()}`;
      string = `${string}${'PAID'.padEnd(nameS + quantityS + taxS)}  ${paid.padEnd(totalS)}\n`;
    }
    if ('due' in order.payment) {
      const due = `₹ ${(order.payment.due / 100).toFixed(2).toString()}`;
      string = `${string}${'DUE'.padEnd(nameS + quantityS + taxS)}  ${due.padEnd(totalS)}\n`;
    }
  }
  string = `${string}\n\nInvoice by Hindpos.com\n\n`;
  const numCopies = config.numCopies || 1;
  const forAwait = async (func, inputs, n) => {
    if (n > 0) {
      await func(...inputs);
      forAwait(func, inputs, (n - 1));
    }
  };
  forAwait(printPost, [
    string,
    {
      pageSize: config.printType,
    },
    {},
    true,
  ], numCopies);
  return 0;
};

const print = async (context, { order, config }) => {
  const isRefund = /(R|r)efund/g.test(order.subType);
  if (config.printType === 'A4' || config.printType === 'A5') {
    if (!isRefund) {
      await pdfPrintOrder(order, config);
    }
  } else {
    if (isRefund) {
      await txtPrintRefund(context, order, config);
    }
    await txtPrintOrder(context, order, config);
  }
};
export default {
  print,
};
