import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';

const reset = async (context, data) => {
  const query = `query {
    reset (user: ${stringify(data, { indent: '  ', singleQuotes: false })}) {
      _id
    }
  }`;
  const result = await gqlQuery(context, query);
  if (result.data.reset) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Settings have been reset.',
      type: 'success',
    });
  }
  if (result.errors) {
    context.commit('setPopup', {
      title: 'Error',
      text: 'Incorrect password.',
      type: 'error',
    });
  }
};

const getConfig = async (context) => {
  const query = `
  query {
    getConfig {
      autoBill
      numCopies
      smallSidebar
      expiryWarn
      printType
      target {
        profit
        revenue
        order
      }
      store {
        gstin
        id
        license
        name
        owner
        phone
      }
      address {
        pincode
        city
        state
        country
        landmark
        lines
      }
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getConfig;
  if (result) {
    await context.commit('setConfig', result);
  }
};

const setConfig = async (context, config) => {
  /* eslint no-param-reassign: ["error", { "props": false }] */
  config.numCopies = Math.max(1, parseInt(`0${config.numCopies}`, 10));
  config.expiryWarn = Math.max(1, parseInt(`0${config.expiryWarn}`, 10));
  config.target.profit = parseInt(`0${config.target.profit}`, 10);
  config.target.revenue = parseInt(`0${config.target.revenue}`, 10);
  config.target.order = parseInt(`0${config.target.order}`, 10);
  const query = `
  mutation {
    setConfig (config: ${stringify(config, { indent: '  ', singleQuotes: false })})
    {
      autoBill
      numCopies
      smallSidebar
      expiryWarn
      printType
      target {
        profit
        revenue
        order
      }
      store {
        gstin
        id
        license
        name
        owner
        phone
      }
      address {
        pincode
        city
        state
        country
        landmark
        lines
      }
    }
  }`;
  const result = (await gqlQuery(context, query)).data.setConfig;
  if (result) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Settings have been saved.',
      type: 'success',
    });
  }
  await context.commit('setConfig', result);
  context.dispatch('getSellStats');
};

export default {
  getConfig,
  setConfig,
  reset,
};
