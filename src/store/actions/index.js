import { shutdownGet } from '@/services/Api';
import Orders from './Orders';
import Stocks from './Stocks';
import QuickOrder from './QuickOrder';
import Products from './Products';
import Settings from './Settings';
import Notification from './Notification';
import Bill from './Bill';
import Accounts from './Accounts';
import Login from './Login';
import Reports from './Reports';

const shutdown = async () => {
  await shutdownGet();
};

export default {
  // Orders
  loadOrderHistory: Orders.history,
  loadOrderDetails: Orders.details,
  loadOrderProducts: Orders.products,
  addOrderCreate: Orders.create,
  discardOrder: Orders.discard,
  refundOrder: Orders.refund,
  paymentOrder: Orders.payment,

  // QuickOrder
  addQuickOrderCreate: QuickOrder.create,

  // Product
  loadGlobalProducts: Products.GlobalProducts,
  loadLocalProducts: Products.LocalProducts,
  createProduct: Products.create,
  updateProduct: Products.update,
  deleteProduct: Products.del,

  // Stocks
  loadStockHistory: Stocks.history,
  loadStockDetails: Stocks.details,
  createStockPageOne: Stocks.createPageOne,
  createStock: Stocks.create,
  loadStockOverview: Stocks.overview,
  paymentStock: Stocks.payment,

  // Notification
  loadExpiryNotification: Notification.productExpiry,
  // Settings
  getConfig: Settings.getConfig,
  setConfig: Settings.setConfig,
  appReset: Settings.reset,

  // System
  shutdown,
  billPrint: Bill.print,

  // Accounts
  accountCreateTransaction: Accounts.create,
  accountCredits: Accounts.credits,
  accountDebits: Accounts.debits,
  accountTransactions: Accounts.allTransactions,
  getSellStats: Accounts.getSellStats,

  // Login
  registerUser: Login.register,
  login: Login.login,
  setUser: Login.update,

  // Report
  generateReport: Reports.generate,
};
