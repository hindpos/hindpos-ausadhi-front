import stringify from 'stringify-object';

import { gqlQuery } from '@/services/Api';

const register = async (context, data) => {
  const query = `
  mutation {
    createUser(user: ${stringify(data, { indent: '  ', singleQuotes: false })}) {
      _id
    }
  }`;
  const out = (await gqlQuery(context, query)).data.createUser;
  if (out) {
    context.commit('setPopup', {
      title: 'Account Created',
      text: `The ${data.admin ? 'admin' : 'non-admin'} account was created succesfully.`,
      type: 'success',
    });
  } else {
    context.commit('setPopup', {
      title: 'Error in Signup',
      text: 'Username alreay exists.',
      type: 'success',
    });
  }
};

const login = async (context, data) => {
  const query = `
  query {
    validateUser(user: ${stringify(data, { indent: '  ', singleQuotes: false })}) {
      _id
      username
      name
      admin
      contacts {
        phones
        emails
      }
      address {
        city
        country
        landmark
        pincode
        state
        lines
      }
      birthDate
      gender
      department
    }
  }`;
  const out = (await gqlQuery(context, query));
  const user = out.data.validateUser;
  if (user) {
    await context.commit('setUser', user);
    await context.commit('setSidebarPages', user);
    context.commit('login');
    context.commit('setPopup', {
      title: 'Success',
      text: 'You have successfully logged in.',
      type: 'success',
    });
  }
  if (out.errors) {
    context.commit('setPopup', {
      title: 'Unauthorized',
      text: 'Invalid username or password.',
      type: 'error',
    });
  }
};

const update = async (context, data) => {
  const query = `
  mutation {
    updateUser(
      user: ${stringify(data.user, { indent: '  ', singleQuotes: false })},
      oldUser: ${stringify(data.oldUser, { indent: '  ', singleQuotes: false })},
    ) {
      _id
      username
      name
      admin
      contacts {
        phones
        emails
      }
      address {
        city
        country
        landmark
        pincode
        state
        lines
      }
      birthDate
      gender
      department
    }
  }`;
  const out = (await gqlQuery(context, query));
  const user = out.data.validateUser;
  if (user) {
    await context.commit('setUser', user);
    context.commit('setPopup', {
      title: 'Success',
      text: 'User profile has been updated successfully.',
      type: 'success',
    });
  }
  if (out.errors) {
    context.commit('setPopup', {
      title: 'Unauthorized',
      text: 'Incorrect password.',
      type: 'error',
    });
  }
  context.commit('setDashboardFromProfile');
};

export default {
  register,
  login,
  update,
};
