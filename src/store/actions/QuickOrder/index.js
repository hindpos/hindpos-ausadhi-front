import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';

const create = async (context, data) => {
  await context.commit('createQuickOrderTransaction', data);
  const query = `
  mutation {
    createTransaction(transaction: ${stringify(context.state.quickOrderTransaction, { indent: '  ', singleQuotes: false })}) {
    _id
    num
    type
    subType
    tax
    discount
    total
    price
    secondPerson {
      name
    }
    person {
      name
      contacts {
        phones
      }
    }
    payment {
      closed
      due
      paid
      type
      history {
        amount
        date
      }
    }
    products {
      name
        batch
        basePrice
        tax
        hsn
        discount
        netPrice
        quantity
        total
        expiry
        costPrice
        sellPrice
        refund {
          discard
          keep
          refund
          amount
        }
    }
    createdAt
    username
    }
  }
  `;
  const result = (await gqlQuery(context, query)).data.createTransaction;
  await context.commit('toggleQuickOrderSubPage', result);
};

export default {
  create,
};
