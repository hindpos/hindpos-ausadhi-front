import _ from 'lodash';
import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';

const GlobalProducts = async (context, data = {}) => {
  const query = `query {
    getGlobalProducts (
      product: {
        ${(data.name) ? `name: {regex: "${data.name.split(' ').join('.*')}", options: "i"}` : ''}
      }
      ${(data.limit) ? `limit: ${data.limit}` : 'limit: 30'}
      ${(data.skip) ? `skip: ${data.skip}` : 'skip: 0'}
    ) {
      _id
      details
      name
      price
      unitPrice
      tax
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getGlobalProducts;
  context.commit('setGlobalProducts', result);
};

const LocalProducts = async (context, data = {}) => {
  const query = `{
    getProducts (
      product: {
        ${(data.name) ? `name: {regex: "${data.name.split(' ').join('.*')}", options: "i"}` : ''}
      }
    ){
      _id
      name
      basePrice
      tax
      discount
      netPrice
      details
      minQuantity
      maxQuantity
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getProducts
    .map((product) => {
      const newProduct = _.cloneDeep(product);
      newProduct.quantity = 1;
      newProduct.id = newProduct._id;
      delete newProduct._id;
      if (newProduct.discount) {
        newProduct.discount = (newProduct.discount / 100).toFixed(2);
      } else {
        newProduct.discount = 0;
      }
      if (newProduct.tax) {
        newProduct.tax = (newProduct.tax / 100).toFixed(0);
      } else {
        newProduct.tax = 0;
      }
      if (newProduct.basePrice) {
        newProduct.basePrice = (newProduct.basePrice / 100).toFixed(2);
      } else {
        newProduct.basePrice = newProduct.netPrice;
        newProduct.tax = 0;
        newProduct.discount = 0;
      }
      if (newProduct.netPrice) {
        newProduct.netPrice = (newProduct.netPrice / 100).toFixed(2);
      } else {
        newProduct.netPrice = (newProduct.basePrice
          * (1 + (newProduct.tax / 100))
          * (1 - (newProduct.discount / 100))).toFixed(2);
      }
      newProduct.total = newProduct.netPrice;
      return newProduct;
    });
  context.commit('setLocalProducts', result);
};

const create = async (context, data) => {
  const product = _.cloneDeep(data);
  product.user = context.state.user._id;
  product.username = context.state.user.username;
  const query = `mutation {
    createProduct(product: ${stringify(product, { indent: '  ', singleQuotes: false })})
    {
      _id
    }
  }`;
  const result = await gqlQuery(context, query);
  if (result.data.createProduct) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Product created successfully.',
      type: 'success',
    });
  }
};

const update = async (context, data) => {
  const product = _.cloneDeep(data);
  const id = product.id;
  delete product.id;
  delete product.total;
  delete product.quantity;
  product.user = context.state.user._id;
  product.username = context.state.user.username;
  const query = `mutation {
    updateProduct(filter: {_id: "${id}"}
      product: ${stringify(product, { indent: '  ', singleQuotes: false })})
    {
      _id
    }
  }`;
  const result = await gqlQuery(context, query);
  if (result.data.updateProduct) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Product updated successfully.',
      type: 'success',
    });
  }
  context.dispatch('loadLocalProducts');
};

const del = async (context, product) => {
  const query = `
  mutation {
    deleteProduct(id: "${product.id}") {
      _id
    }
  }
  `;
  if (context.state.stockOverview &&
    (context.state.stockOverview.total.products
      .map(p => p.id._id).indexOf(product.id) >= 0)) {
    context.commit('setPopup', {
      title: 'Can\'t delete product',
      text: 'Products of this kind are remaining in stock.',
      type: 'error',
    });
  } else {
    const result = await gqlQuery(context, query);
    if (result.data) {
      context.commit('setPopup', {
        title: 'Success',
        text: 'Product deleted successfully.',
        type: 'success',
      });
    }
    await LocalProducts(context);
  }
};

export default {
  GlobalProducts,
  LocalProducts,
  create,
  update,
  del,
};
