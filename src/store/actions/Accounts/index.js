import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';
import _ from 'lodash';

const credits = async (context, data = {}) => {
  const queryString = `
  ${(data.id) ? `num: {eq: ${data.id}}` : ''}
  ${(data.minPrice || data.maxPrice) ? `total: {
    ${(data.maxPrice) ? `lte: ${data.maxPrice}00` : ''}
    ${(data.minPrice) ? `gte: ${data.minPrice}00` : ''}
  }` : ''}
  ${(context.state.user.admin) ?
    `${(data.username) ? `username: {regex: "${data.username.split(' ').join('.*')}", options: "i"}` : ''}`
    : `username: {regex: "${context.state.user.username}", options: "i"}`}
  ${(data.startDate || data.endDate) ? `createdAt: {
    ${(data.startDate) ? `gte: "${new Date(data.startDate).toISOString()}"` : ''}
    ${(data.endDate) ? `lte: "${new Date(data.endDate).toISOString()}"` : ''}
  }` : ''}
  `;
  const typeMap = {
    'Credit | Order': 'order|credit',
    'Order Create': 'order',
    Credit: 'credit',
  };
  const query = `query {
    getTransactions(transaction: {
      ${queryString}
      type: {regex: "(${typeMap[data.subType]})"}, subType: {nin: ["discard", "refund"]}}, limit: ${data.limit || 0}, skip: ${data.skip || 0}) {
      _id
      createdAt
      description
      num
      subType
      total
      type
      username
    }
  }`;
  const result = (await gqlQuery(context, query));
  if (result.data) {
    context.commit('setAccountsCredits', result.data.getTransactions);
  }
};

const debits = async (context, data = {}) => {
  const queryString = `
  ${(data.id) ? `num: {eq: ${data.id}}` : ''}
  ${(data.minPrice || data.maxPrice) ? `total: {
    ${(data.maxPrice) ? `lte: ${data.maxPrice}00` : ''}
    ${(data.minPrice) ? `gte: ${data.minPrice}00` : ''}
  }` : ''}
  ${(context.state.user.admin) ?
    `${(data.username) ? `username: {regex: "${data.username.split(' ').join('.*')}", options: "i"}` : ''}`
    : `username: {regex: "${context.state.user.username}", options: "i"}`}
  ${(data.startDate || data.endDate) ? `createdAt: {
    ${(data.startDate) ? `gte: "${new Date(data.startDate).toISOString()}"` : ''}
    ${(data.endDate) ? `lte: "${new Date(data.endDate).toISOString()}"` : ''}
  }` : ''}
  `;
  const typeMap = {
    'Debit | Stock': 'debit|stock',
    'Stock Create': 'stock',
    Debit: 'debit',
  };
  const query = `query {
    getTransactions(transaction: {
      ${queryString}
      type: {regex: "(${typeMap[data.subType]})"}, subType: {nin: ["discard", "refund"]}}, limit: ${data.limit || 10}, skip: ${data.skip || 0}) {
      _id
      createdAt
      description
      num
      subType
      total
      type
      username
    }
  }`;
  const result = (await gqlQuery(context, query));
  if (result.data) {
    context.commit('setAccountsDebits', result.data.getTransactions);
  }
};

const allTransactions = async (context, data = {}) => {
  const query = `query {
    getTransactions(skip: ${data.skip || 0}, limit: ${data.limit || 0}) {
      _id
      createdAt
      updatedAt
      refundAmount
      refundDate
      description
      num
      subType
      total
      type
      username
    }
  }`;
  const result = (await gqlQuery(context, query));
  if (result.data) {
    const transactions = result.data.getTransactions;
    const l = transactions.length;
    for (let i = 0; i < l; i += 1) {
      if (transactions[i].subType === 'refund') {
        const refTrans = JSON.parse(JSON.stringify(transactions[i]));
        transactions[i].subType = 'create';
        refTrans.total = transactions[i].refundAmount;
        refTrans.createdAt = transactions[i].refundDate;
        transactions.push(refTrans);
      }
    }
    transactions.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
    const isCredit = (t) => {
      if (t.type === 'credit') return true;
      if ((t.type === 'order') && (t.subType === 'create')) return true;
      if ((t.type === 'stock') && (t.subType === 'refund')) return true;
      return false;
    };
    const isDebit = (t) => {
      if (t.type === 'debit') return true;
      if ((t.type === 'stock') && (t.subType === 'create')) return true;
      if ((t.type === 'order') && (t.subType === 'refund')) return true;
      return false;
    };
    const creditTrans = transactions.filter(isCredit);
    const debitTrans = transactions.filter(isDebit);
    context.commit('setAccountsTransactions', transactions);
    context.commit('setAccountsCredits', creditTrans);
    context.commit('setAccountsDebits', debitTrans);
    context.dispatch('getSellStats');
  }
};

const getSellStats = async (context) => {
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  const weekAgo = new Date(today);
  weekAgo.setDate(weekAgo.getDate() - 7);
  const obtain = `{
    _id
    createdAt
    num
    subType
    total
    type
    username
    products {
      id
      name
      quantity
      total
      costPrice
      sellPrice
      refund {
        refund
      }
    }
  }`;
  const weekAgoQuery = `query {
    getTransactions(transaction: {
      type: {eq: "order"},
      subType: {nin: ["discard"]},
      createdAt: {gte: "${weekAgo.toISOString()}", lte: "${today.toISOString()}"}
    }) ${obtain}
  }`;
  const weekAgoData = (await gqlQuery(context, weekAgoQuery));

  const todayQuery = `query {
    getTransactions(transaction: {
      type: {eq: "order"},
      subType: {nin: ["discard"]},
      createdAt: {gte: "${today.toISOString()}"}
    }) ${obtain}
  }`;
  const todayData = (await gqlQuery(context, todayQuery));

  const paiseToRupees = paise => parseFloat((paise / 100).toFixed(2));
  const getAllProducts = (orders) => {
    const tempOrders = JSON.parse(JSON.stringify(orders));
    const allProducts = [];
    for (let i = 0; i < tempOrders.length; i += 1) {
      for (let j = 0; j < tempOrders[i].products.length; j += 1) {
        tempOrders[i].products[j].createdAt = tempOrders[i].createdAt;
        tempOrders[i].products[j].profit =
        tempOrders[i].products[j].sellPrice - tempOrders[i].products[j].costPrice;
        allProducts.push(tempOrders[i].products[j]);
      }
    }
    return allProducts;
  };

  const getMedProfit = (products) => {
    const profits = [];
    const ids = [];
    products.forEach((product) => {
      const quantity = product.quantity - ((product.refund && product.refund.refund) || 0);
      const idx = ids.indexOf(product.id);
      if (idx < 0) {
        profits.push({ name: product.name, y: product.profit * quantity });
        ids.push(product.id);
      } else {
        profits[idx].y += product.profit * quantity;
      }
    });
    const totProfit = profits.map(p => p.y).reduce((x, y) => x + y, 0);
    for (let i = 0; i < profits.length; i += 1) {
      profits[i].y /= totProfit;
      profits[i].y *= 100;
      profits[i].y = parseFloat(profits[i].y, 10);
    }
    const prunedProfits = _.sortBy(profits, 'y');
    prunedProfits.reverse();
    if (prunedProfits.length > 4) {
      const other = prunedProfits.splice(3);
      const sumOther = other.map(o => o.y).reduce((acc, nxt) => acc + nxt);
      prunedProfits.push({ name: 'Other', y: sumOther });
    }
    return prunedProfits;
  };

  const getDailyStats = (products, oneDay = false) => {
    const profits = [];
    const revenues = [];
    const orders = [];
    const dates = [];
    products.forEach((product) => {
      const sellDate = new Date(product.createdAt);
      sellDate.setHours(0, 0, 0, 0);
      const sellDateStr = sellDate.toISOString();
      const idx = dates.indexOf(sellDateStr);
      const quantity = product.quantity - ((product.refund && product.refund.refund) || 0);
      if (idx < 0) {
        profits.push(parseInt(product.profit * quantity, 10));
        revenues.push(parseInt(product.sellPrice * quantity, 10));
        orders.push(1);
        dates.push(sellDateStr);
      } else {
        profits[idx] += parseInt(product.profit * quantity, 10);
        revenues[idx] += parseInt(product.sellPrice * quantity, 10);
        orders[idx] += 1;
      }
    });
    if (oneDay) {
      return {
        profit: profits[0] || 0,
        revenue: revenues[0] || 0,
        order: orders[0] || 0,
        date: dates[0] || today.toISOString(),
      };
    }
    return {
      profits,
      revenues,
      orders,
      dates,
    };
  };
  const getMeterData = (salesData, config) => {
    const mtDat = {};
    mtDat.profit = {};
    mtDat.revenue = {};
    mtDat.order = {};

    mtDat.profit.val = paiseToRupees(salesData.todayStats.profit);
    mtDat.revenue.val = paiseToRupees(salesData.todayStats.revenue);
    mtDat.order.val = salesData.todayStats.order;

    mtDat.profit.target = parseInt(config.target.profit || 0, 10);
    mtDat.revenue.target = parseInt(config.target.revenue || 0, 10);
    mtDat.order.target = parseInt(config.target.order || 0, 10);

    mtDat.profit.percentage = (mtDat.profit.val >= mtDat.profit.target) ? 1
      : (mtDat.profit.val / mtDat.profit.target);
    mtDat.revenue.percentage = (mtDat.revenue.val >= mtDat.revenue.target) ? 1
      : (mtDat.revenue.val / mtDat.revenue.target);
    mtDat.order.percentage = (mtDat.order.val >= mtDat.order.target) ? 1
      : (mtDat.order.val / mtDat.order.target);

    if (salesData.weekStats) {
      if (salesData.weekStats.dates.length >= 1) {
        mtDat.profit.change = parseInt(((mtDat.profit.val
          / ((paiseToRupees(salesData.weekStats.profits[0])) + 0.01)) - 1) * 100, 0);
        mtDat.revenue.change = parseInt(((mtDat.revenue.val
          / ((paiseToRupees(salesData.weekStats.revenues[0])) + 0.01)) - 1) * 100, 0);
        mtDat.order.change = parseInt(((mtDat.order.val
          / (salesData.weekStats.orders[0] + 0.001)) - 1) * 100, 0);

        mtDat.profit.projection = paiseToRupees(salesData.weekStats.profits
          .reduce((a, c) => a + c) / salesData.weekStats.dates.length);
        mtDat.revenue.projection = paiseToRupees(salesData.weekStats.revenues
          .reduce((a, c) => a + c) / salesData.weekStats.dates.length);
        mtDat.order.projection = (salesData.weekStats.orders
          .reduce((a, c) => a + c) / salesData.weekStats.dates.length).toFixed(2);
      } else {
        mtDat.profit.change = 0;
        mtDat.revenue.change = 0;
        mtDat.order.change = 0;

        mtDat.profit.projection = 0;
        mtDat.revenue.projection = 0;
        mtDat.order.projection = 0;
      }
    }
    return mtDat;
  };
  const bufferArray = (array, finalIndex, gen = () => 0) => {
    while (array.length < finalIndex) {
      array.push(gen(array.length));
    }
    return array.map(x => x / 100).reverse();
  };
  const getChartData = (salesData) => {
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const chDat = {};
    chDat.pie = salesData.medProfitShare;
    chDat.bar = {};
    const allRevenues = (salesData.weekStats.revenues.slice());
    allRevenues.unshift(salesData.todayStats.revenue);
    const allProfits = (salesData.weekStats.profits.slice());
    allProfits.unshift(salesData.todayStats.profit);
    chDat.bar.revenue = bufferArray(allRevenues, 8);
    chDat.bar.revenue.shift();
    chDat.bar.profit = bufferArray(allProfits, 8);
    chDat.bar.profit.shift();
    chDat.bar.date = (days.slice(today.getDay() + 1)).concat(days.slice(0, today.getDay() + 1));
    return chDat;
  };
  if (todayData.data) {
    const stats = {};
    const weekAgoProducts = getAllProducts(weekAgoData.data.getTransactions);
    const todayProducts = getAllProducts(todayData.data.getTransactions);
    const allProducts = weekAgoProducts.concat(todayProducts);
    stats.medProfitShare = getMedProfit(allProducts);
    stats.weekStats = getDailyStats(weekAgoProducts);
    stats.todayStats = getDailyStats(todayProducts, true);
    context.commit('setSellStats', stats);
    const meterData = getMeterData(stats, context.state.config);
    const chartData = getChartData(stats);
    context.commit('setMeterData', meterData);
    context.commit('setChartData', chartData);
  }
};

const create = async (context, data) => {
  const copyData = _.cloneDeep(data);

  copyData.user = context.state.user._id;
  copyData.username = context.state.user.username;

  const query = `mutation {
    createTransaction(transaction: ${stringify(copyData, { indent: '  ', singleQuotes: false })}) {
      _id
      createdAt
      description
      num
      total
      type
      username
    }
  }`;

  const result = (await gqlQuery(context, query));

  if (result.data.createTransaction) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Your accounts have been updated.',
      type: 'success',
    });
    return result.data.createTransaction;
  }
  return 0;
};

export default {
  create,
  credits,
  debits,
  allTransactions,
  getSellStats,
};
