import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';

const generate = async (context, data) => {
  const query = `query {
    report (report: ${stringify(data, { indent: '  ', singleQuotes: false })}){
      success
    }
  }`;
  const out = await gqlQuery(context, query);
  if (out.data.report.success) {
    context.commit('setPopup', {
      title: 'Report generated',
      text: 'Report has been generated.',
      type: 'success',
    });
  }
};

export default {
  generate,
};
