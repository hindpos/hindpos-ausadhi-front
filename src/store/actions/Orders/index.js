
import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';
import _ from 'lodash';

import Bill from '../Bill';

const history = async (context, data = {}) => {
  const queryString = `
  ${(data.id) ? `num: {eq: ${data.id}}` : ''}
  ${(data.minPrice || data.maxPrice) ? `total: {
    ${(data.maxPrice) ? `lte: ${data.maxPrice}00` : ''}
    ${(data.minPrice) ? `gte: ${data.minPrice}00` : ''}
  }` : ''}
  ${(context.state.user.admin) ?
    `${(data.username) ? `username: {regex: "${data.username.split(' ').join('.*')}", options: "i"}` : ''}`
    : `username: {regex: "${context.state.user.username}", options: "i"}`}
  ${(data.subType) ? `subType: {regex: "${data.subType.split(' ').join('.*')}", options: "i"}` : ''}
  ${(data.startDate || data.endDate) ? `createdAt: {
    ${(data.startDate) ? `gte: "${new Date(data.startDate).toISOString()}"` : ''}
    ${(data.endDate) ? `lte: "${new Date(data.endDate).toISOString()}"` : ''}
  }` : ''}
  `;
  const query = `
  { getTransactions(transaction: {
      type: {
        eq: "order"
      }
      ${queryString}
    }
    limit: ${data.limit || 10}) {
      _id
      createdAt
      num
      subType
      username
      total
  }}`;
  const result = (await gqlQuery(context, query)).data.getTransactions;
  context.commit('setOrderHistory', result);
};

const details = async (context, id) => {
  const query = `
  {
    getTransactions (transaction: {
      _id: "${id}"
    }) {
      _id
      createdAt
      discount
      num
      payment {
        closed
        due
        paid
        history {
          amount
          date
          username
        }
      }
      person {
        address {
          country
          landmark
          lines
          pincode
          state
          city
        }
        contacts {
          phones
          emails
        }
        name
        age
        birthDate
        gender
      }
      secondPerson {
        license
        name
      }
      price
      tax
      total
      username
      type
      subType
      products {
        name
        batch
        basePrice
        tax
        hsn
        discount
        netPrice
        quantity
        total
        expiry
        costPrice
        sellPrice
        refund {
          discard
          keep
          refund
          amount
        }
      }
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getTransactions[0];
  context.commit('setOrderHistorySubPageFromSearch', result);
};

const create = async (context, data) => {
  await context.commit('setOrderCreateCusfoTransaction', data);
  const query = `
  mutation {
    createTransaction(transaction: ${stringify(context.state.orderCreateTransaction, { indent: '  ', singleQuotes: false })}) {
    _id
    num
    type
    subType
    tax
    discount
    total
    price
    secondPerson {
      license
      name
    }
    person {
      age
      birthDate
      gender
      name
      address {
        country
        landmark
        pincode
        state
        lines
        city
      }
      contacts {
        emails
        phones
      }
    }
    payment {
      closed
      details
      due
      paid
      type
      history {
        username
        amount
        date
      }
    }
    products {
      id
      name
      hsn
      basePrice
      netPrice
      tax
      discount
      quantity
      total
      expiry
      batch
    }
    createdAt
    updatedAt
    username
    }
  }
  `;
  const result = (await gqlQuery(context, query)).data.createTransaction;
  await context.commit('resetOrderCreateTransaction', result);
  await context.commit('setOrderCreateSubPageFromCusFo');
};

const discard = async (context, products) => {
  const transaction = {};
  transaction.products = _.cloneDeep(products);
  transaction.discount = 0;
  transaction.tax = 0;
  transaction.price = 0;
  transaction.total = products
    .map(product => product.total)
    .reduce((a, c) => a + c);
  transaction.type = 'order';
  transaction.subType = 'discard';
  transaction.user = context.state.user._id;
  transaction.username = context.state.user.username;
  const query = `
  mutation {
    createTransaction(transaction: ${stringify(transaction, { indent: '  ', singleQuotes: false })}) {
    _id
    }
  }`;
  const result = await gqlQuery(context, query);
  if (result.data.createTransaction) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Products discarded successfully.',
      type: 'success',
    });
  }
  await context.dispatch('loadStockOverview');
  await context.dispatch('accountTransactions');
};

const products = async (context, id) => {
  const query = `
  query {
    getTransactions (transaction: { _id: "${id}" }){
      _id
      num
      type
      subType
      tax
      discount
      total
      price
      secondPerson {
        license
        name
      }
      person {
        age
        birthDate
        gender
        name
        address {
          country
          landmark
          pincode
          state
          lines
        }
        contacts {
          emails
          phones
        }
      }
      payment {
        closed
        details
        due
        paid
        type
        history {
          username
          amount
          date
        }
      }
      products {
        id
        name
        hsn
        basePrice
        netPrice
        tax
        discount
        quantity
        total
        expiry
        batch
        costPrice
        sellPrice
        refund {
          refund
          discard
          keep
        }
      }
      username
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getTransactions[0];
  context.commit('setRefundOrder', result);
};

const update = async (context, order, isRefund) => {
  const query = `
  mutation {
    updateTransaction(filter: {
      _id: "${order._id}"
    }, transaction: ${stringify(order, { indent: '  ', singleQuotes: false })},
    isRefund: ${isRefund}) {
      _id
    }
  }`;
  const result = await gqlQuery(context, query);
  return result;
};

const refund = async (context, data) => {
  const order = data[0];
  const config = data[1];
  const result = await update(context, order, 1);
  if (result.data.updateTransaction) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Products refunded successfully.',
      type: 'success',
    });
  }
  if (config.autoBill) {
    await Bill.print(context, { order, config });
  }
  context.commit('setRefundOrder', null);
  await context.dispatch('loadStockOverview');
  // await context.dispatch('loadOrderHistory');
  await context.dispatch('accountTransactions');
};

const payment = async (context, order) => {
  const result = await update(context, order, 0);
  if (result.data.updateTransaction) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Payment updated.',
      type: 'success',
    });
  }
  context.commit('setOrderHistorySubPageFromDetails');
  await context.dispatch('loadOrderHistory');
};

export default {
  create,
  history,
  details,
  discard,
  products,
  refund,
  payment,
};
