// import _ from 'lodash';
import stringify from 'stringify-object';
import { gqlQuery } from '@/services/Api';
import _ from 'lodash';

const history = async (context, data = {}) => {
  const queryString = `
  ${(data.stockId) ? `num: {eq: ${data.stockId}}` : ''}
  ${(data.minPrice || data.maxPrice) ? `total: {
    ${(data.maxPrice) ? `lte: ${data.maxPrice}00` : ''}
    ${(data.minPrice) ? `gte: ${data.minPrice}00` : ''}
  }` : ''}
  ${(data.distributor) ? `person: { name: {regex: "${data.distributor.split(' ').join('.*')}", options: "i"} }` : ''}
  ${(context.state.user.admin) ?
    `${(data.username) ? `username: {regex: "${data.username.split(' ').join('.*')}", options: "i"}` : ''}`
    : `username: {regex: "${context.state.user.username}", options: "i"}`}
  ${(data.startDate || data.endDate) ? `createdAt: {
    ${(data.startDate) ? `gte: "${new Date(data.startDate).toISOString()}"` : ''}
    ${(data.endDate) ? `lte: "${new Date(data.endDate).toISOString()}"` : ''}
  }` : ''}
  `;
  const query = `
  query { getTransactions(transaction: {
      type: {
        eq: "stock"
      }
      ${queryString}
    }
    limit: ${data.limit || 10}) {
      _id
      createdAt
      num
      username
      person {
        name
      }
      total
  }}`;
  const result = (await gqlQuery(context, query)).data.getTransactions;
  context.commit('setStockHistory', result);
};

const details = async (context, id) => {
  const query = `
  query {
    getTransactions (transaction: {
      _id: "${id}"
    }) {
      _id
      createdAt
      discount
      num
      payment {
        closed
        due
        paid
        history {
          amount
          date
          username
        }
      }
      person {
        address {
          country
          landmark
          lines
          pincode
          state
          city
        }
        contacts {
          phones
          emails
        }
        name
        company
      }
      secondPerson {
        license
        name
      }
      price
      tax
      total
      username
      type
      subType
      products {
        name
        batch
        basePrice
        tax
        hsn
        discount
        netPrice
        quantity
        total
        expiry
        costPrice
        sellPrice
        refund {
          discard
          keep
          refund
          amount
        }
      }
    }
  }`;
  const result = (await gqlQuery(context, query)).data.getTransactions[0];
  // console.log(result);
  context.commit('setStockHistorySubPageFromSearch', result);
};

const createPageOne = async (context, data) => {
  const { products, receiptNum, distributor, gstin, state } = data;
  const transaction = {};
  transaction.products = _.cloneDeep(products);
  transaction.discount = products
    .map(product => ((product.discount * product.basePrice) * product.quantity) / 10000)
    .map(discount => parseInt(discount, 10))
    .reduce((a, c) => a + c);
  transaction.tax = products
    .map(product => ((product.tax
      * (product.basePrice * (1 - (product.discount / 10000)))) * product.quantity) / 10000)
    .map(tax => parseInt(tax, 10))
    .reduce((a, c) => a + c);
  transaction.total = products
    .map(product => product.total)
    .reduce((a, c) => a + c);
  transaction.price = transaction.total - transaction.tax;
  transaction.type = 'stock';
  transaction.subType = 'create';
  transaction.user = context.state.user._id;
  transaction.username = context.state.user.username;
  if (receiptNum) {
    transaction.receiptNum = receiptNum;
  }
  if (distributor || gstin) {
    const person = {};
    if (distributor) {
      person.name = distributor;
    }
    if (gstin) {
      person.gstin = gstin;
    }
    person.address = { state };
    if (state !== context.state.config.address.state) {
      transaction.isInterState = true;
    }
    transaction.person = person;
    transaction.payment = {};
    transaction.payment.type = 'cash';
    transaction.payment.closed = true;
    transaction.payment.history = [];
    transaction.payment.history[0] = {
      amount: transaction.total,
      date: (new Date()).toISOString(),
      user: context.state.user._id,
      username: context.state.user.username,
    };
    transaction.payment.paid = transaction.total;
    transaction.payment.due = 0;
  }
  context.commit('setStockCreateTransaction', transaction);
  if (distributor || gstin) {
    context.dispatch('createStock', transaction);
  } else {
    context.commit('setStockCreateSubPageFromCreate');
  }
};

const create = async (context, data) => {
  await context.commit('setStockCreateDisFoTransaction', data);
  const query = `
  mutation {
    createTransaction(transaction: ${stringify(context.state.stockCreateTransaction, { indent: '  ', singleQuotes: false })}) {
    _id
    num
    type
    subType
    tax
    discount
    total
    price
    person {
      age
      birthDate
      gender
      name
      company
      address {
        country
        landmark
        pincode
        state
        city
        lines
      }
      contacts {
        emails
        phones
      }
    }
    products {
      id
      name
      basePrice
      netPrice
      tax
      hsn
      discount
      quantity
      total
      expiry
      batch
    }
    createdAt
    username
    }
  }
  `;
  const result = (await gqlQuery(context, query)).data.createTransaction;
  context.commit('resetStockCreateTransaction', result);
  context.commit('setStockCreateSubPagefromDisFo');
  context.dispatch('loadStockOverview');
  context.dispatch('loadStockHistory');
  context.dispatch('accountTransactions');
};

const overview = async (context) => {
  const query = `
  query {
    getCurrentState {
      stocks {
        total
        products {
          _id
          id
          name
          batch
          quantity
          basePrice
          tax
          hsn
          discount
          netPrice
          expiry
          costPrice
          sellPrice
          total
        }
      }
      total {
        total
        products {
          id
          name
          quantity
          basePrice
          tax
          netPrice
          total
        }
      }
    }
  }
  `;
  const result = (await gqlQuery(context, query)).data.getCurrentState;
  await context.commit('setStockOverview', result);
  let products = [];
  if (result) {
    const tempStocks = JSON.parse(JSON.stringify(result.stocks));
    let idx = 0;
    products = tempStocks.reduce((acc, nxt) => {
      const reqProducts = nxt.products.map((product) => {
        const reqProduct = _.pick(product.id, ['_id', 'name', 'minQuantity', 'maxQuantity', 'details.prescription']);
        reqProduct.id = reqProduct._id;
        delete reqProduct._id;
        reqProduct.visible = true;
        _.extend(reqProduct, _.pick(product, ['_id', 'batch', 'expiry', 'quantity', 'costPrice', 'sellPrice', 'discount', 'tax', 'hsn']));
        reqProduct.costPrice = (reqProduct.costPrice / 100).toFixed(2);
        reqProduct.sellPrice = (reqProduct.sellPrice / 100).toFixed(2);
        reqProduct.netPrice = reqProduct.sellPrice;
        reqProduct.tax = (reqProduct.tax / 100).toFixed(0);
        reqProduct.discount = (reqProduct.discount / 100).toFixed(2);
        reqProduct.basePrice = (reqProduct.sellPrice
          / (1 + (reqProduct.tax / 100))
          / (1 - (reqProduct.discount / 100))).toFixed(2);
        reqProduct.idx = idx;
        idx += 1;
        return reqProduct;
      });
      return _.concat(acc, reqProducts);
    }, []);
  } else {
    products = [];
  }
  await context.commit('setInStockProducts', products);
};

const update = async (context, stock, isRefund) => {
  const query = `
  mutation {
    updateTransaction(filter: {
      _id: "${stock._id}"
    }, transaction: ${stringify(stock, { indent: '  ', singleQuotes: false })},
    isRefund: ${isRefund}) {
      _id
    }
  }`;
  const result = await gqlQuery(context, query);
  return result;
};

const payment = async (context, stock) => {
  const result = await update(context, stock, 0);
  if (result.data.updateTransaction) {
    context.commit('setPopup', {
      title: 'Success',
      text: 'Payment updated.',
      type: 'success',
    });
  }
  context.commit('setOrderHistorySubPageFromDetails');
  await context.dispatch('loadOrderHistory');
};

export default {
  createPageOne,
  create,
  history,
  details,
  overview,
  payment,
};
