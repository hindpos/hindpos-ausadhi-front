import _ from 'lodash';

const productExpiry = async (context) => {
  await context.dispatch('loadStockOverview');
  const notification = _.cloneDeep(context.state.notification);
  context.state.inStockProducts.forEach((product) => {
    if (product.expiry) {
      const today = new Date();
      const expiryDate = new Date(product.expiry);
      const timeDiff = expiryDate - today;
      const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      if (diffDays <= context.state.config.expiryWarn && diffDays > 0) {
        notification.push({
          title: 'Product gonna expire',
          content: `${product.name} from batch ${product.batch} will expire in ${diffDays} day(s).`,
        });
      } else if (diffDays < 0) {
        notification.push({
          title: 'Product has expired',
          content: `${product.name} from batch ${product.batch} has expired ${-diffDays} day(s) ago.`,
        });
      }
    }
  });
  context.state.stockOverview.total.products.forEach((product) => {
    if (product.id.minQuantity && product.quantity < product.id.minQuantity) {
      notification.push({
        title: 'Product less than minimum',
        content: `${product.quantity} ${product.name} left.`,
      });
    } else if (product.id.maxQuantity && product.quantity > product.id.maxQuantity) {
      notification.push({
        title: 'Product more than maximum',
        content: `${product.quantity} ${product.name} are now in stock.`,
      });
    }
  });

  if (notification.length > context.state.notification.length) {
    context.commit('setPopup', {
      title: 'Alert',
      text: 'You have notifications that need attention.',
      type: 'warn',
    });
  }
  context.commit('setNotification', notification);
};

export default {
  productExpiry,
};

